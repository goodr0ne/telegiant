package com.goodrone.widgets.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.http.client.*;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.user.client.ui.*;

/**
 *
 */
@SuppressWarnings("unused")
public class WelcomeWidget implements EntryPoint {
  final FlexTable table = new FlexTable();
  final FlexTable errors = new FlexTable();
  final FlexTable editBox = new FlexTable();
  final TextBox editNameText = new TextBox();
  final TextBox editTelephoneText = new TextBox();
  final FlexTable addBox = new FlexTable();
  final TextBox addNameText = new TextBox();
  final TextBox addTelephoneText = new TextBox();
  final FlexTable searchBox = new FlexTable();
  final TextBox searchKeywords = new TextBox();

  /**
   *
   */
  private void updateEditBox(String id, String name, String telephone) {
    editNameText.setText(name.replace("\"", ""));
    editTelephoneText.setText(telephone.replace("\"", ""));
    editBox.setTitle(id);
    editBox.setWidget(0, 2, new Button("Change", new ClickHandler() {
      public void onClick(ClickEvent event) {
        editTelephone(editBox.getTitle(), editNameText.getText(),
                editTelephoneText.getText());
      }
    }));
    editBox.setVisible(true);
  }

  /**
   *
   */
  private void updateErrorBox(String error) {
    int cnt = errors.getRowCount();
    if (cnt > 10) {
      cnt = 1;
      errors.removeAllRows();
      errors.setText(0, 0, "--Id--");
      errors.setText(0, 1, "-----------Errors----Spotted-----------");
    }
    if (cnt < 1) {
      cnt = 1;
      errors.setText(0, 0, "--Id--");
      errors.setText(0, 1, "-----------Errors----Spotted-----------");
    }
    errors.setText(cnt, 0, "  " + cnt + "  ");
    errors.setText(cnt, 1, error);
  }

  /**
   *
   */
  private void updateTable(JSONArray list) {
    try {
      table.removeAllRows();
      table.setText(0, 0, "--Id--");
      table.setText(0, 1, "-------------Name-------------");
      table.setText(0, 2, "------Telephone------");
      table.setText(0, 3, "--X--");
      if (list.size() > 0) {
        for (int i = 0; i < list.size(); i++) {
          JSONObject object = list.get(i).isObject();
          final String id = object.get("id").toString();
          final String name = object.get("name").toString();
          final String telephone = object.get("telephone").toString();
          table.setWidget(1 + i, 0, new Button(id, new ClickHandler() {
            public void onClick(ClickEvent event) {
              updateEditBox(id, name, telephone);
            }
          }));
          table.setWidget(1 + i, 1, new Button(name.replace("\"", ""),
                  new ClickHandler() {
            public void onClick(ClickEvent event) {
              updateEditBox(id, name, telephone);
            }
          }));
          table.setWidget(1 + i, 2, new Button(telephone.replace("\"", ""),
                  new ClickHandler() {
            public void onClick(ClickEvent event) {
              updateEditBox(id, name, telephone);
            }
          }));
          table.setWidget(1 + i, 3, new Button("X", new ClickHandler() {
            public void onClick(ClickEvent event) {
              deleteTelephone(id);
            }
          }));
        }
      }
    } catch (Exception e) {
      updateErrorBox(e.toString());
    }
  }

  /**
   *
   */
  private void editTelephone(String id, String name, String telephone) {
    String url = "http://127.0.0.1:8888/change?id=" + id + "&name=" + name
            + "&telephone=" + telephone;
    RequestBuilder builder = new RequestBuilder(RequestBuilder.GET,
            URL.encode(url));
    try {
      Request request = builder.sendRequest(null, new RequestCallback() {
        public void onError(Request request, Throwable exception) {
          updateErrorBox("can not connect to servlet container");
        }
        public void onResponseReceived(Request request, Response response) {
          JSONObject result;
          try {
            result = JSONParser.parseLenient(response.getText()).isObject();
          } catch (Exception e) {
            result = new JSONObject();
          }
          if (result.containsKey("error")) {
            updateErrorBox(result.get("error").toString());
          } else {
            retrieveTable();
            editBox.setVisible(false);
          }
        }
      });
    } catch (RequestException e) {
      updateErrorBox("error with servlet container request action");
    }
  }

  /**
   *
   */
  private void addTelephone(String name, String telephone) {
    String url = "http://127.0.0.1:8888/add?name=" + name
            + "&telephone=" + telephone;
    RequestBuilder builder = new RequestBuilder(RequestBuilder.GET,
            URL.encode(url));
    try {
      Request request = builder.sendRequest(null, new RequestCallback() {
        public void onError(Request request, Throwable exception) {
          updateErrorBox("can not connect to servlet container");
        }
        public void onResponseReceived(Request request, Response response) {
          JSONObject result;
          try {
            result = JSONParser.parseLenient(response.getText()).isObject();
          } catch (Exception e) {
            result = new JSONObject();
          }
          if (result.containsKey("error")) {
            updateErrorBox(result.get("error").toString());
          } else {
            retrieveTable();
          }
        }
      });
    } catch (RequestException e) {
      updateErrorBox("error with servlet container request action");
    }
  }

  /**
   *
   */
  private void deleteTelephone(String id) {
    String url = "http://127.0.0.1:8888/delete?id=" + id;
    RequestBuilder builder = new RequestBuilder(RequestBuilder.GET,
            URL.encode(url));
    try {
      Request request = builder.sendRequest(null, new RequestCallback() {
        public void onError(Request request, Throwable exception) {
          updateErrorBox("can not connect to servlet container");
        }
        public void onResponseReceived(Request request, Response response) {
          JSONObject result;
          try {
            result = JSONParser.parseLenient(response.getText()).isObject();
          } catch (Exception e) {
            result = new JSONObject();
          }
          if (result.containsKey("error")) {
            updateErrorBox(result.get("error").toString());
          } else {
            retrieveTable();
          }
        }
      });
    } catch (RequestException e) {
      updateErrorBox("error with servlet container request action");
    }
  }

  /**
   *
   */
  private void searchTable(String keywords) {
    String url = "http://127.0.0.1:8888/search?keywords=" + keywords;
    RequestBuilder builder = new RequestBuilder(RequestBuilder.GET,
            URL.encode(url));
    try {
      Request request = builder.sendRequest(null, new RequestCallback() {
        public void onError(Request request, Throwable exception) {
          updateErrorBox("can not connect to servlet container");
        }
        public void onResponseReceived(Request request, Response response) {
          try {
            updateTable(JSONParser.parseLenient(response.getText())
                    .isObject().get("list").isArray());
          } catch (Exception e) {
            updateErrorBox(e.toString());
          }
        }
      });
    } catch (RequestException e) {
      updateErrorBox("error with servlet container request action");
    }
  }

  /**
   *
   */
  private void retrieveTable() {
    String url = "http://127.0.0.1:8888/all";
    RequestBuilder builder = new RequestBuilder(RequestBuilder.GET,
            URL.encode(url));
    try {
      Request request = builder.sendRequest(null, new RequestCallback() {
        public void onError(Request request, Throwable exception) {
          updateErrorBox("can not connect to servlet container");
        }
        public void onResponseReceived(Request request, Response response) {
          try {
            updateTable(JSONParser.parseLenient(response.getText())
                    .isObject().get("list").isArray());
          } catch (Exception e) {
            updateErrorBox(e.toString());
          }
        }
      });
    } catch (RequestException e) {
      updateErrorBox("error with servlet container request action");
    }
  }

  /**
   *
   */
  public void onModuleLoad() {
    final Button updateButton = new Button("Update", new ClickHandler() {
      public void onClick(ClickEvent event) {
        retrieveTable();
      }
    });
    RootPanel.get().add(updateButton);
    RootPanel.get().add(table);
    editBox.setVisible(false);
    editBox.setWidget(0, 0, editNameText);
    editBox.setWidget(0, 1, editTelephoneText);
    editBox.setTitle("0");
    RootPanel.get().add(editBox);
    addBox.setWidget(0, 0, addNameText);
    addBox.setWidget(0, 1, addTelephoneText);
    addBox.setWidget(0, 2, new Button("Add", new ClickHandler() {
      public void onClick(ClickEvent event) {
        addTelephone(addNameText.getText(), addTelephoneText.getText());
      }
    }));
    RootPanel.get().add(addBox);
    searchBox.setWidget(0, 1, searchKeywords);
    searchBox.setWidget(0, 2, new Button("Search", new ClickHandler() {
      public void onClick(ClickEvent event) {
        searchTable(searchKeywords.getText());
      }
    }));
    RootPanel.get().add(searchBox);
    RootPanel.get().add(errors);
  }
}