package com.goodrone;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.StringUtils;

import java.sql.*;

/**
 *
 * @author Yegor Scherbatkin
 */
public class Reader {
  String dbUrl = "jdbc:postgresql://localhost/jerry?" +
          "user=tommy&password=1";

  /**
   *
   */
  public Reader() {

  }

  /**
   *
   */
  public static final Reader instance = new Reader();

  /**
   *
   */
  public static Reader getInstance() {
    return instance;
  }

  /**
   *
   */
  private JsonArray getAllList() {
    try {
      Connection connection = DriverManager.getConnection(dbUrl);
      String query = "SELECT * FROM telephones ORDER BY id;";
      PreparedStatement statement = connection.prepareStatement(query);
      ResultSet set = statement.executeQuery();
      JsonArray list = new JsonArray();
      while (set.next()) {
        try {
          JsonObject object = new JsonObject();
          object.addProperty("id", set.getInt(1));
          object.addProperty("name", set.getString(2));
          object.addProperty("telephone", set.getString(3));
          list.add(object);
        } catch (Exception e) {
          set.close();
          statement.close();
          connection.close();
          return list;
        }
      }
      set.close();
      statement.close();
      connection.close();
      return list;
    } catch (Exception e) {
      return new JsonArray();
    }
  }

  /**
   *
   */
  private boolean checkTelephoneString(String telephone) {
    return (!StringUtils.isBlank(telephone)
            && telephone.matches("[+][0-9]{12}"));
  }

  /**
   *
   */
  private boolean checkNameString(String name) {
    if (StringUtils.isBlank(name)) {
      return false;
    }
    if (name.matches("[A-Z][a-zA-Z]*[ ]*[A-Za-z]*")) {
      if (name.length() < 50) {
        return true;
      }
    }
    return false;
  }

  /**
   *
   */
  @SuppressWarnings("ConstantConditions")
  private JsonObject getOneObject(String parameter, int type) {
    JsonArray list = getAllList();
    if (list.size() < 1) {
      return new JsonObject();
    }
    if (type == 0) {
      for (int i = 0; i < list.size(); i++) {
        JsonObject object = list.get(i).getAsJsonObject();
        if (object.get("id").getAsString().equals(parameter)) {
          return object;
        }
      }
      return new JsonObject();
    } else {
      return new JsonObject();
    }
  }

  /**
   *
   */
  private boolean checkObjectExist(long id) {
    return !getOneObject("" + id, 0).isJsonNull();
  }

  /**
   *
   */
  private boolean isDuplicate(String telephone, String name) {
    JsonArray list = getAllList();
    if (list.size() < 1) {
      return false;
    }
    for (int i = 0; i < list.size(); i++) {
      JsonObject object = list.get(i).getAsJsonObject();
      String telephoneOld = object.get("telephone").getAsString();
      String nameOld = object.get("name").getAsString();
      if (nameOld.equals(name) && telephoneOld.equals(telephone)) {
        return true;
      }
    }
    return false;
  }

  /**
   *
   */
  private synchronized JsonElement addObject(String telephone,
                                             String name, int newId) {
    try {
      JsonArray list = getAllList();
      int id;
      if (newId > 0) {
        id = newId;
      } else {
        if (list.size() < 1) {
          id = 1;
        } else {
          int maxId = 0;
          for (int i = 0; i < list.size(); i++) {
            int nextId = list.get(i).getAsJsonObject().get("id").getAsInt();
            if (nextId > maxId) {
              maxId = nextId;
            }
          }
          id = maxId + 1;
        }
      }
      Connection connection = DriverManager.getConnection(dbUrl);
      String query = "INSERT INTO telephones (id,name,telephone) "
              + "VALUES (" + id + ", '" + name + "', '" + telephone + "');";
      PreparedStatement statement = connection.prepareStatement(query);
      statement.execute();
      statement.close();
      connection.close();
      return new JsonObject();
    } catch (Exception e) {
      return new Gson().toJsonTree(e);
    }
  }

  /**
   *
   */
  public JsonObject add(String telephone, String name, int id) {
    JsonObject object = new JsonObject();
    if (StringUtils.isBlank(telephone)) {
      object.addProperty("error", "telephone parameter is blank " +
              "or not specified");
      return object;
    }
    telephone = telephone.replace(" ", "+");
    if (StringUtils.isBlank(name)) {
      object.addProperty("error", "name parameter is blank " +
              "or not specified");
      return object;
    }
    if (!checkNameString(name)) {
      object.addProperty("error", "name parameter is not a proper string, " +
              "should be one or two words, letters only, started with " +
              "uppercase, maximum length is 50 characters, " +
              "example: Clint Eastwood, Smokey, SomeOtherName");
      return object;
    }
    if (!checkTelephoneString(telephone)) {
      object.addProperty("error", "telephone parameter is not a proper " +
              "string, example: +123456789012");
      object.addProperty("telephone", telephone);
      return object;
    }
    if (isDuplicate(telephone, name)) {
      object.addProperty("error", "that telephone - name pair " +
              "already exists in database");
      return object;
    }
    JsonElement tryAdd = addObject(telephone, name, id);
    if (!tryAdd.toString().equals("{}")) {
      object.addProperty("error", "some error occurred during " +
              "adding new name - telephone pair");
      object.add("debug", tryAdd);
      return object;
    }
    return object;
  }

  /**
   *
   */
  public JsonObject all() {
    JsonObject object = new JsonObject();
    JsonArray list = getAllList();
    object.add("list", list);
    return object;
  }

  /**
   *
   */
  public JsonObject search(String keywords) {
    JsonObject object = new JsonObject();
    if (StringUtils.isBlank(keywords)) {
      return all();
    }
    JsonArray list = getAllList();
    if (list.size() > 0) {
      keywords = keywords.replace("+", "");
      JsonArray temp = new JsonArray();
      if (StringUtils.isNumeric(keywords)) {
        for (int i = 0; i < list.size(); i++) {
          JsonObject obj = list.get(i).getAsJsonObject();
          String telephone = obj.get("telephone").getAsString();
          if (telephone.contains(keywords)) {
            temp.add(obj);
          }
        }
        list = temp;
      } else {
        for (int i = 0; i < list.size(); i++) {
          JsonObject obj = list.get(i).getAsJsonObject();
          String name = obj.get("name").getAsString();
          if (name.contains(keywords)) {
            temp.add(obj);
          }
        }
        list = temp;
      }
    }
    object.add("list", list);
    return object;
  }

  /**
   *
   */
  public JsonObject delete(String idString) {
    JsonObject object = new JsonObject();
    if (StringUtils.isBlank(idString)) {
      object.addProperty("error", "id parameter is blank " +
              "or not specified");
      return object;
    }
    int id;
    try {
      id = Integer.parseInt(idString);
    } catch (Exception e) {
      object.addProperty("error", "id parameter is not an integer");
      return object;
    }
    if (checkObjectExist(id)) {
      try {
        Connection connection = DriverManager.getConnection(dbUrl);
        String query = "DELETE FROM telephones WHERE id=" + id + ";";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.execute();
        statement.close();
        connection.close();
      } catch (SQLException e) {
        object.add("error", new Gson().toJsonTree(e));
        return object;
      }
    }
    return object;
  }

  /**
   *
   */
  public JsonObject initialize() {
    JsonObject object = new JsonObject();
    try {
      Class.forName("org.postgresql.Driver");
    } catch (ClassNotFoundException e) {
      object.add("error", new Gson().toJsonTree(e));
      return object;
    }
    try {
      Connection connection = DriverManager.getConnection(dbUrl);
      String query = "CREATE TABLE IF NOT EXISTS telephones " +
              "(id serial PRIMARY KEY, name VARCHAR(50), " +
              "telephone VARCHAR(13));";
      PreparedStatement statement = connection.prepareStatement(query);
      statement.execute();
      statement.close();
      connection.close();
    } catch (SQLException e) {
      object.add("error", new Gson().toJsonTree(e));
      return object;
    }
    return object;
  }

  /**
   *
   */
  public JsonObject change(String idString, String telephone, String name) {
    JsonObject object = new JsonObject();
    if (StringUtils.isBlank(idString)) {
      object.addProperty("error", "id parameter is blank " +
              "or not specified");
      return object;
    }
    int id;
    try {
      id = Integer.parseInt(idString);
    } catch (Exception e) {
      object.addProperty("error", "id parameter is not an integer");
      return object;
    }
    JsonObject oldObj = getOneObject("" + id, 0);
    if (oldObj.isJsonNull()) {
      object.addProperty("error", "telephone - name pair with that id " +
              "is not exists");
      return object;
    }
    if (StringUtils.isBlank(telephone)) {
      object.addProperty("error", "telephone parameter is blank " +
              "or not specified");
      return object;
    }
    telephone = telephone.replace(" ", "+");
    if (StringUtils.isBlank(name)) {
      object.addProperty("error", "name parameter is blank " +
              "or not specified");
      return object;
    }
    if (!checkNameString(name)) {
      object.addProperty("error", "name parameter is not a proper string, " +
              "should be one or two words, letters only, started with " +
              "uppercase, maximum length is 50 characters, " +
              "example: Clint Eastwood, Smokey, SomeOtherName");
      return object;
    }
    if (!checkTelephoneString(telephone)) {
      object.addProperty("error", "telephone parameter is not a proper " +
              "string, example: +123456789012");
      return object;
    }
    String oldTelephone = oldObj.get("telephone").getAsString();
    String oldName = oldObj.get("name").getAsString();
    if (oldName.equals(name) && oldTelephone.equals(telephone)) {
      object.addProperty("error", "both telephone and name " +
              "are the same as the old ones");
      return object;
    }
    if (isDuplicate(telephone, name)) {
      object.addProperty("error", "new telephone - name pair matched " +
              "with already existed one");
      return object;
    }
    JsonObject debugObj = delete("" + id);
    if (!debugObj.has("error")) {
      debugObj = add(telephone, name, id);
      if (debugObj.has("error")) {
        object.addProperty("error",
                "some error occurred during adding new entry");
        object.add("debug", debugObj);
        return object;
      }
    } else {
      object.addProperty("error",
              "some error occurred during deleting old entry");
      object.add("debug", debugObj);
      return object;
    }
    return object;
  }
}