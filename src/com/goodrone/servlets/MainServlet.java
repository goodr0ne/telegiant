package com.goodrone.servlets;

import com.goodrone.Reader;
import com.google.gson.JsonObject;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Yegor Scherbatkin
 */
public class MainServlet extends HttpServlet {
  Reader reader = Reader.getInstance();

  /**
   *
   */
  protected void doGet(HttpServletRequest req, HttpServletResponse res)
          throws IOException {
    this.process(req, res);
  }

  /**
   *
   */
  protected void doPost(HttpServletRequest req, HttpServletResponse res)
          throws IOException {
    this.process(req, res);
  }

  /**
   *
   */
  private void process(HttpServletRequest request,
                       HttpServletResponse response) throws IOException {
    try {
      response.setStatus(200);
      PrintWriter out = response.getWriter();
      response.setContentType("text/plain");
      String url = request.getRequestURL().toString();
      String[] urlWords = url.split("/");
      String action = urlWords[urlWords.length - 1];
      JsonObject object = new JsonObject();
      if (!action.equals("add") && !action.equals("delete")
              && !action.equals("all") && !action.equals("search")
              && !action.equals("initialize")
              && !action.equals("change") && !action.equals("index")) {
        object.addProperty("error", "not supported action");
        out.println(object);
        out.close();
      }
      if (action.equals("add")) {
        object = reader.add(request.getParameter("telephone"),
                request.getParameter("name"), 0);
      }
      if (action.equals("delete")) {
        object = reader.delete(request.getParameter("id"));
      }
      if (action.equals("search")) {
        object = reader.search(request.getParameter("keywords"));
      }
      if (action.equals("all")) {
        object = reader.all();
      }
      if (action.equals("initialize")) {
        object = reader.initialize();
      }
      if (action.equals("change")) {
        object = reader.change(request.getParameter("id"),
                request.getParameter("telephone"),
                request.getParameter("name"));
      }
      if (action.equals("index")) {
        object = reader.initialize();
        object.addProperty("message", "Welcome To TeleGiant!");
      }
      out.println(object);
      out.close();
    }
    catch (Exception e) {
      response.setStatus(200);
      PrintWriter out = response.getWriter();
      response.setContentType("text/plain");
      JsonObject object = new JsonObject();
      object.addProperty("error", e.toString());
      out.println(object);
      out.close();
    }
  }
}